//
// Created by danandla on 3/15/23.
//

#include <sqlParser.h>
#include <ast.h>
#include "lexer.h"
#include "parser.h"

int parse_query(const char *query, AstNode** result) {
    yy_scan_string(query);
    int code = yyparse(result);
    yylex_destroy();
    return code;
}
