#include <stdio.h>
#include <stdlib.h>
#include <ast.h>
#include <string.h>
#include <sqlParser.h>

#define MAX_LIMIT 200

int main() {
//    char *tmp;
//
//    AstColumnList *colList = new(&_AstColumnList, (int) AST_COLUMN_LIST, 1,
//                                 (int) AST_COLUMN, "column", 1, "table");
//    AstColumn *column = new(&_AstColumn, (int) AST_COLUMN, "column2", 1, "table2");
//    colList->push(colList, column);
//    delete(column);
//
//    AstColumns *columns = new(&_AstColumns, (int) AST_COLUMNS, 1, 0, colList);
//    AstColumns *columns2 = new(&_AstColumns, (int) AST_COLUMNS, 1, 1);
//    AstTable *table = new(&_AstTable, (int) AST_TABLE, "asf");
//
//    AstJoin *firstJoin = new(&_AstJoin, (int) AST_JOIN, (int) INT32_T,
//                             (int) AST_TABLE, "left",
//                             (int) AST_TABLE, table->name,
//                             (int) AST_COLUMN, "lCol", 1, "lTable",
//                             (int) AST_COLUMN, "rCol", 1, "rTable"
//    );
//
//    AstJoinList *jList = new(&_AstJoinList, (int) AST_JOIN_LIST, 1,
//                             (int) AST_JOIN, (int) firstJoin->columnType,
//                             (int) AST_TABLE, "left",
//                             (int) AST_TABLE, firstJoin->right->name,
//                             (int) AST_COLUMN, firstJoin->leftColumn->columnName, 1, firstJoin->leftColumn->tableName,
//                             (int) AST_COLUMN, firstJoin->rightColumn->columnName, 1,
//                             firstJoin->rightColumn->tableName);
//
//    delete(firstJoin);
//    AstSrcTable *srcTable = new(&_AstSrcTable, (int) AST_SRC_TABLE, 1, table, 1, jList);
//
//    AstStatementColumn* colSt = new(&_AstStatementColumn, AST_STATEMENT, COLUMN, AST_COLUMN, "sdf", 1, "voyage");
//
//    AstValue* val = new(&_AstValue, AST_VALUE, INT32_T, 2452);
//    AstStatementImm* immSt = new(&_AstStatementImm, AST_STATEMENT, IMMVAL, val);
//    AstStatementUnary* unSt = new(&_AstStatementUnary, AST_STATEMENT, UNARY, 1, NEQ, colSt);
//
//    AstStatementBinary* binSt1 = new(&_AstStatementBinary, AST_STATEMENT, BINARY, 1, NEQ,
//                                    immSt, unSt);
//    AstStatementBinary* binSt2 = new(&_AstStatementBinary, AST_STATEMENT, BINARY, 1, NEQ,
//                                     immSt, immSt);
//    AstStatementBinary* binSt = new(&_AstStatementBinary, AST_STATEMENT, BINARY, 1, NEQ,
//                                     binSt1, binSt2);
//
//    delete(colSt);
//    AstSelect *select = new(&_AstSelect, (int) AST_SELECT, 1,
//                            columns, srcTable, 1, binSt);
//
//
//    delete(val);
//    delete(binSt1);
//    delete(binSt2);
//    delete(binSt);
//    delete(immSt);
//    delete(unSt);
//    delete(columns);
//    delete(columns2);
//    delete(table);
//    delete(srcTable);
//    delete(colList);
//    delete(jList);
//
//    tmp = select->toString(select);
//    delete(select);
//    printf("%s\n", tmp);
//    free(tmp);


    char query[] = "select * from olympiad;";
    char str[MAX_LIMIT];
    while (fgets(str, MAX_LIMIT, stdin)) {
        if (strcmp(str, "exit\n") == 0) return 0;

        AstNode *astNode;
        int code = parse_query(str, &astNode);
        if (code == 0) {
            char *tmp = callToString(astNode);
            char *wr = astNode->toStringExtend(astNode, tmp);
            printf("%s\n", wr);
            free(wr);
            free(tmp);
            callDelete(astNode);
        }
    }
    return 0;
}
