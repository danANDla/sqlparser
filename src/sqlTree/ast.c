//
// Created by danandla on 3/15/23.
//

#include <ast.h>
#include <mytypes.h>
#include <inttypes.h>
#include <string.h>
#include <stdio.h>

char *concat(char *s1, const char *s2) {
    char *buffer = malloc(strlen(s1) + 1);
    strcpy(buffer, s1);
    s1 = realloc(s1, strlen(buffer) + strlen(s2) + 1);
    strcpy(s1, buffer);
    strcat(s1, s2);
    free(buffer);
    return s1;
}


/* -----------------------------------------------------------------
 * AST node
 */
char *AstNodeToStringExtend(AstNode *self, const char *str) {
    char *type = getAstTypeStr(self->type);
    char start[] = "{ astType: ";
    char *ret = malloc(sizeof(char) * (1 + strlen(start)));
    strcpy(ret, start);
    ret = concat(ret, type);
    ret = concat(ret, ", ");
    ret = concat(ret, str);
    ret = concat(ret, " }");
    free(type);
    return ret;
}

void AstNode_ctor(void *_self, va_list *app) {
    AstNode *self = _self;
    int type = va_arg(*app, int);
    self->type = (astType) type;
    self->toStringExtend = AstNodeToStringExtend;
}

void AstNode_dtor(void *_self) {
    AstNode *self = _self;
}

/* -----------------------------------------------------------------
 * AST column
 */
char *AstColumn_toString(AstColumn *self) {
    char *buff;
    char start[] = "columnName: ";
    buff = malloc(sizeof(char) + (strlen(start) + 1));
    strcpy(buff, start);
    buff = concat(buff, self->columnName);

    if (self->isTableNamed) {
        buff = concat(buff, ", isTableNamed: true, tableName: ");
        buff = concat(buff, self->tableName);
    } else buff = concat(buff, ", isTableNamed: false");

    return buff;
}

void AstColumn_ctor(void *_self, va_list *app) {
    AstColumn *self = _AstNode.ctor(_self, app);

    char *columnNameTmp = va_arg(*app, char*);
    int isTableNamed = va_arg(*app, int);
    char *columnName = malloc(strlen(columnNameTmp) + 1);
    strcpy(columnName, columnNameTmp);
    if (isTableNamed) {
        char *tableNameT = va_arg(*app, char*);
        char *tableName = malloc(strlen(tableNameT) + 1);
        strcpy(tableName, tableNameT);
        self->tableName = tableName;
        self->isTableNamed = true;
    } else self->isTableNamed = false;
    self->columnName = columnName;
    self->toString = AstColumn_toString;
}

void *AstColumn_clone(const void *_self) {
    const AstColumn *self = _self;
    AstColumn *ret;
    if (self->isTableNamed) {
        ret = new(&_AstColumn, (int) AST_COLUMN, self->columnName, 1, self->tableName);
    } else {
        ret = new(&_AstColumn, (int) AST_COLUMN, self->columnName, 0);
    }
    return ret;
}

void AstColumn_dtor(void *_self) {
    AstColumn *self = _self;
    if (self->isTableNamed) free(self->tableName);
    free(self->columnName);
    _AstNode.dtor(_self);
}

/* -----------------------------------------------------------------
 * AST columns list
 */
char *AstColumnList_toString(AstColumnList *self) {
    char start[] = "columns: [";
    char *ret = malloc(sizeof(char) + strlen(start) + 1);
    strcpy(ret, start);
    for (uint16_t i = 0; i < self->columnsNumber - 1; ++i) {
        ret = concat(ret, "{");
        char *t = (*self->list[i]).toString(self->list[i]);
        ret = concat(ret, t);
        free(t);
        ret = concat(ret, "}, ");
    }
    ret = concat(ret, "{");
    char *t = (*self->list[self->columnsNumber - 1]).toString(self->list[self->columnsNumber - 1]);
    ret = concat(ret, t);
    free(t);
    ret = concat(ret, "}]");
    return ret;
}

void *AstColumnList_clone(void *_self) {
    AstColumnList *self = _self;

    AstColumnList *ret = calloc(1, _AstColumnList.size);
    *(const struct Class **) ret = &_AstColumnList;

    uint16_t columnsNumber = self->columnsNumber;
    AstColumn **cols = malloc(sizeof(AstColumn **) * columnsNumber);
    for (uint16_t i = 0; i < columnsNumber; ++i) {
        cols[i] = _AstColumn.clone(self->list[i]);
    }

    ret->list = cols;
    ret->columnsNumber = columnsNumber;
    ret->toString = AstColumnList_toString;
    return ret;
}

void AstColumnListPush(AstColumnList *self, AstColumn *newColumn) {
    AstColumnList *joinList = self;
    uint16_t columnsNumber = joinList->columnsNumber + 1;
    AstColumn **joins = realloc(self->list, sizeof(AstJoin *) * columnsNumber);
    self->columnsNumber = columnsNumber;
    joins[columnsNumber - 1] = _AstColumn.clone(newColumn);
    self->list = joins;
}

void AstColumnList_ctor(void *_self, va_list *app) {
    AstColumnList *self = _AstNode.ctor(_self, app);

    int isNew = va_arg(*app, int);
    uint16_t columnsNumber;
    AstColumn **cols;
    if (!isNew) {
        AstColumnList *colList = va_arg(*app, void *);
        columnsNumber = colList->columnsNumber + 1;
        cols = malloc(sizeof(AstColumn *) * columnsNumber);
        for (uint16_t i = 0; i < columnsNumber - 1; ++i) {
            cols[i] = _AstColumn.clone(colList->list[i]);
        }
        delete(colList);
    } else {
        columnsNumber = 1;
        cols = malloc(sizeof(AstColumn *) * columnsNumber);
    }

    AstColumn *newColumn = calloc(1, _AstColumn.size);
    *(const struct Class **) newColumn = &_AstColumn;
    _AstColumn.ctor(newColumn, app);

    cols[columnsNumber - 1] = newColumn;
    self->list = cols;
    self->columnsNumber = columnsNumber;

    self->toString = AstColumnList_toString;
    self->push = AstColumnListPush;
}

void AstColumnList_dtor(void *_self) {
    AstColumnList *self = _self;
    for (uint16_t i = 0; i < self->columnsNumber; ++i) {
        delete(self->list[i]);
    }
    free(self->list);
    _AstNode.dtor(_self);
}

/* -----------------------------------------------------------------
 * AST columns
 */
char *AstColumns_toString(AstColumns *self) {
    char start[] = "isAll: ";
    char *ret = malloc(sizeof(char) * strlen(start) + 1);
    strcpy(ret, start);
    if (self->isAll) {
        ret = concat(ret, "true");
    } else {
        ret = concat(ret, "false, ");
        char *t = self->columns->toString(self->columns);
        ret = concat(ret, t);
        free(t);
    }
    return ret;
}

void AstColumns_immCtor(AstColumns *self, va_list *app) {
    int isAll = va_arg(*app, int);
    if (isAll) {
        self->isAll = true;
    } else {
        self->isAll = false;

        AstColumnList *newList = calloc(1, _AstColumnList.size);
        *(const struct Class **) newList = &_AstColumnList;
        _AstColumnList.ctor(newList, app);

        self->columns = newList;
    }
    self->toString = AstColumns_toString;
}

void AstColumns_objCtor(AstColumns *self, va_list *app) {
    int isAll = va_arg(*app, int);
    if (isAll) {
        self->isAll = true;
    } else {
        self->isAll = false;
        AstColumnList *colList = va_arg(*app, void*);
        self->columns = _AstColumnList.clone(colList);
    }
    self->toString = AstColumns_toString;
}

void AstColumns_ctor(void *_self, va_list *app) {
    AstColumns *self = _AstNode.ctor(_self, app);
    int ctorVariant = va_arg(*app, int);
    if (ctorVariant == 1) {
        AstColumns_objCtor(self, app);
    } else {
        AstColumns_immCtor(self, app);
    }
}

void *AstColumns_clone(const void *_self) {
    const AstColumns *self = _self;

    AstColumns* ret;

    if (self->isAll) {
        ret = new(&_AstColumns, (int) AST_COLUMNS, 1);
        ret->isAll = true;
    } else {
        ret = new(&_AstColumns, (int) AST_COLUMNS, 1, 0, self->columns);
        ret->isAll = false;
    }
    ret->toString = AstColumns_toString;
    return ret;
}

void AstColumns_dtor(void *_self) {
    AstColumns *self = _self;
    if (!self->isAll) { delete(self->columns); }
    _AstNode.dtor(_self);
}

/* -----------------------------------------------------------------
 * AST table
 */
char *AstTable_toString(AstTable *self) {
    char start[] = "tableName: ";
    char *ret = malloc(sizeof(char) * strlen(start) + 1);
    strcpy(ret, start);
    ret = concat(ret, self->name);
    return ret;
}

void *AstTable_clone(void *_self) {
    const AstTable *self = _self;
    AstTable* ret = new(&_AstTable, (int) AST_TABLE, self->name);
    ret->toString = AstTable_toString;
    return ret;
}

void AstTable_ctor(void *_self, va_list *app) {
    AstTable *self = _AstNode.ctor(_self, app);

    char *tableNameTmp = va_arg(*app, char*);
    char *tableName = malloc(strlen(tableNameTmp) + 1);
    strcpy(tableName, tableNameTmp);
    self->name = tableName;

    self->toString = AstTable_toString;
}

void AstTable_dtor(void *_self) {
    AstTable *self = _self;
    free(self->name);
    _AstNode.dtor(_self);
}

/* -----------------------------------------------------------------
 * AST join
 */
char *AstJoin_toString(AstJoin *self) {
    char start[] = "cmpColumnDataType: ";
    char *ret = malloc(sizeof(char) * strlen(start) + 1);
    strcpy(ret, start);
    char *tmp = getDataTypeStr(self->columnType);
    ret = concat(ret, tmp);
    free(tmp);

    ret = concat(ret, ", leftTable: {");
    tmp = self->left->toString(self->left);
    ret = concat(ret, tmp);
    free(tmp);

    ret = concat(ret, "}, rightTable: {");
    tmp = self->right->toString(self->right);
    ret = concat(ret, tmp);
    free(tmp);

    ret = concat(ret, "}, leftColumn: {");
    tmp = self->leftColumn->toString(self->leftColumn);
    ret = concat(ret, tmp);
    free(tmp);

    ret = concat(ret, "}, rightColumn: {");
    tmp = self->rightColumn->toString(self->rightColumn);
    ret = concat(ret, tmp);
    free(tmp);
    ret = concat(ret, "}");

    return ret;
}

void AstJoin_ctor(void *_self, va_list *app) {
    AstJoin *self = _AstNode.ctor(_self, app);

    int type = va_arg(*app, int);
    self->columnType = (dataType) type;

    AstTable *lTable = calloc(1, _AstTable.size);
    *(const struct Class **) lTable = &_AstTable;
    _AstTable.ctor(lTable, app);

    AstTable *rTable = calloc(1, _AstTable.size);
    *(const struct Class **) rTable = &_AstTable;
    _AstTable.ctor(rTable, app);

    AstColumn *lColumn = calloc(1, _AstColumn.size);
    *(const struct Class **) lColumn = &_AstColumn;
    _AstColumn.ctor(lColumn, app);

    AstColumn *rColumn = calloc(1, _AstColumn.size);
    *(const struct Class **) rColumn = &_AstColumn;
    _AstColumn.ctor(rColumn, app);

    self->left = lTable;
    self->right = rTable;
    self->leftColumn = lColumn;
    self->rightColumn = rColumn;

    self->toString = AstJoin_toString;
}

void *AstJoin_clone(const void *_self) {
    const AstJoin *self = _self;
    AstJoin *ret;
    ret = new(&_AstJoin, (int) AST_JOIN,
              (int) self->columnType,
              (int) AST_TABLE, self->left->name,
              (int) AST_TABLE, self->right->name,
              (int) AST_COLUMN, self->leftColumn->columnName, 1, self->leftColumn->tableName,
              (int) AST_COLUMN, self->rightColumn->columnName, 1, self->rightColumn->tableName);
    return ret;
}

void AstJoin_dtor(void *_self) {
    AstJoin *self = _self;
    delete(self->left);
    delete(self->right);
    delete(self->leftColumn);
    delete(self->rightColumn);
    _AstNode.dtor(_self);
}

/* -----------------------------------------------------------------
 * AST joins list
 */
char *AstJoinList_toString(AstJoinList *self) {
    char start[] = "joins: [";
    char *ret = malloc(sizeof(char) + strlen(start) + 1);
    strcpy(ret, start);
    for (uint16_t i = 0; i < self->joinsNumber - 1; ++i) {
        ret = concat(ret, "{");
        char *t = (*self->list[i]).toString(self->list[i]);
        ret = concat(ret, t);
        free(t);
        ret = concat(ret, "},");
    }
    ret = concat(ret, "{");
    char *t = (*self->list[self->joinsNumber - 1]).toString(self->list[self->joinsNumber - 1]);
    ret = concat(ret, t);
    free(t);
    ret = concat(ret, "}]");
    return ret;
}

void AstJoinListPush(AstJoinList *self, AstJoin *newJoin) {
    AstJoinList *joinList = self;
    uint16_t joinsNumber = joinList->joinsNumber + 1;
    AstJoin **joins = realloc(self->list, sizeof(AstJoin *) * joinsNumber);
    self->joinsNumber = joinsNumber;
    joins[joinsNumber - 1] = _AstJoin.clone(newJoin);
    self->list = joins;
}

void *AstJoinList_clone(const void *_self) {
    const AstJoinList *self = _self;
    AstJoinList *ret = calloc(1, _AstJoinList.size);
    *(const struct Class **) ret = &_AstJoinList;

    ret->base.type = AST_JOIN_LIST;
    ret->base.toStringExtend = AstNodeToStringExtend;
    ret->joinsNumber = self->joinsNumber;
    ret->toString = AstJoinList_toString;
    ret->push = AstJoinListPush;

    ret->list = malloc(sizeof(AstJoin *) * ret->joinsNumber);
    for (uint16_t i = 0; i < ret->joinsNumber; ++i) {
        ret->list[i] = AstJoin_clone(self->list[i]);
    }
    return ret;
}

void AstJoinList_ctor(void *_self, va_list *app) {
    AstJoinList *self = _AstNode.ctor(_self, app);

    int isNew = va_arg(*app, int);
    uint16_t joinsNumber;
    AstJoin **joins;
    if (!isNew) {
        AstJoinList *joinList = va_arg(*app, void *);
        joinsNumber = joinList->joinsNumber + 1;
        joins = malloc(sizeof(AstJoin *) * joinsNumber);
        for (uint16_t i = 0; i < joinsNumber - 1; ++i) {
            joins[i] = _AstJoin.clone(joinList->list[i]);
        }
        delete(joinList);
    } else {
        joinsNumber = 1;
        joins = malloc(sizeof(AstJoin *) * joinsNumber);
    }

    AstJoin *newJoin = calloc(1, _AstJoin.size);
    *(const struct Class **) newJoin = &_AstJoin;
    _AstJoin.ctor(newJoin, app);

    joins[joinsNumber - 1] = newJoin;
    self->list = joins;
    self->joinsNumber = joinsNumber;

    self->toString = AstJoinList_toString;
    self->push = AstJoinListPush;
}

void AstJoinList_dtor(void *_self) {
    AstJoinList *self = _self;
    for (uint16_t i = 0; i < self->joinsNumber; ++i) {
        delete(self->list[i]);
    }
    free(self->list);
    _AstNode.dtor(_self);
}

/* -----------------------------------------------------------------
 * AST sourceTable
 */
char *AstSrcTable_toString(AstSrcTable *self) {
    char start[] = "srcTable: {";
    char *ret = malloc(sizeof(char) * strlen(start) + 1);
    strcpy(ret, start);
    char *tmp = self->src->toString(self->src);
    ret = concat(ret, tmp);
    free(tmp);
    ret = concat(ret, "}");
    if (self->isJoin) {
        ret = concat(ret, ", joins: {");
        tmp = self->joinList->toString(self->joinList);
        ret = concat(ret, tmp);
        free(tmp);
        ret = concat(ret, "}");
    }
    return ret;
}


void AstSrcTable_immCtor(AstSrcTable *self, va_list *app) {
    AstTable *newTable = calloc(1, _AstTable.size);
    *(const struct Class **) newTable = &_AstTable;
    _AstTable.ctor(newTable, app);

    self->src = newTable;

    int isJoin = va_arg(*app, int);

    if (isJoin == 1) {
        self->isJoin = true;
        AstJoinList *newList = calloc(1, _AstJoinList.size);
        *(const struct Class **) newList = &_AstJoinList;
        _AstJoinList.ctor(newList, app);

        self->joinList = newList;
    } else {
        self->isJoin = false;
    }

    self->toString = AstSrcTable_toString;
}

void AstSrcTable_objCtor(AstSrcTable *self, va_list *app) {
    AstTable *newTable = (AstTable *) va_arg(*app, void*);
    self->src = _AstTable.clone(newTable);
    int isJoin = va_arg(*app, int);

    if (isJoin == 1) {
        self->isJoin = true;
        AstJoinList *newList = (AstJoinList *) va_arg(*app, void*);
        self->joinList = _AstJoinList.clone(newList);
    } else {
        self->isJoin = false;
    }
    self->toString = AstSrcTable_toString;
}

void AstSrcTable_ctor(void *_self, va_list *app) {
    AstSrcTable *self = _AstNode.ctor(_self, app);
    int ctorOption = va_arg(*app, int);
    if (ctorOption == 1) {
        AstSrcTable_objCtor(self, app);
    } else AstSrcTable_immCtor(self, app);
}

void *AstSrcTable_clone(void *_self) {
    const AstSrcTable *self = _self;

//    AstSrcTable *ret = calloc(1, _AstColumns.size);
//    *(const struct Class **) ret = &_AstSrcTable;
    AstSrcTable* ret;

    if (self->isJoin) {
        ret = new(&_AstSrcTable, (int) AST_SRC_TABLE, 1, self->src, 1, self->joinList);
        ret->isJoin = true;
    } else {
        ret = new(&_AstSrcTable, (int) AST_SRC_TABLE, 1, self->src, 0);
        ret->isJoin = false;
    }
    ret->toString = AstSrcTable_toString;
    return ret;
}

void AstSrcTable_dtor(void *_self) {
    AstSrcTable *self = _self;
    if (self->isJoin) {
        _AstJoinList.dtor(self->joinList);
        free(self->joinList);
    }
    delete(self->src);
    _AstNode.dtor(_self);
}

/* -----------------------------------------------------------------
 * AST statement
 */
char *AstStatement_toString(AstStatement *self) {
    char start[] = "statement: ";
    char *ret = malloc(sizeof(char) * strlen(start) + 1);
    strcpy(ret, start);
    char *tmp = getStatementTypeStr(self->type);
    ret = concat(ret, tmp);
    free(tmp);
    return ret;
}

void AstStatement_ctor(void *_self, va_list *app) {
    AstStatement *self = _AstNode.ctor(_self, app);
    self->type = (statementType) va_arg(*app, int);
    self->toString = AstStatement_toString;
}

void AstStatement_dtor(void *_self) {
    _AstNode.dtor(_self);
}


/* -----------------------------------------------------------------
 * AST
 */
char *getImmDataStr(dataType dtype, void *d) {
    char *ret;
    switch (dtype) {
        case STRING_T: {
            sscanf((char *) d, "%ms", &ret);
            break;
        }
        case INT32_T: {
            int32_t i = *((int32_t *) d);
            ret = calloc(1, sizeof(char) * 15);
            sprintf(ret, "%d", i);
            break;
        }
        case FLOAT_T: {
            float f = *((float *) d);
            ret = calloc(1, sizeof(char) * 15);
            sprintf(ret, "%f", f);
            break;
        }
        case BOOL_T: {
            bool b = *((bool *) d);
            if (b) {
                ret = malloc(sizeof(char) * 5);
                strcpy(ret, "true");
            } else {
                ret = malloc(sizeof(char) * 6);
                strcpy(ret, "false");
            }
            break;
        }
    }
    return ret;
}

char *AstValue_toString(AstValue *self) {
    char start[] = "dType: ";
    char *ret = malloc(sizeof(char) * strlen(start) + 1);
    strcpy(ret, start);
    char* tmp = getDataTypeStr(self->dtype);
    ret = concat(ret, tmp);
    free(tmp);
    ret = concat(ret, ", data: ");
    char *data = getImmDataStr(self->dtype, self->data);
    ret = concat(ret, data);
    free(data);
    return ret;
}
void* AstValue_clone(void *_self) {
    AstValue* self = (AstValue*) _self;
    AstValue *ret;
    switch (self->dtype){
        case STRING_T:{
            ret = new(&_AstValue, (int) AST_VALUE, (int) self->dtype, self->data);
            break;
        }
        case INT32_T:{
            int n = *((int*) self->data);
            ret = new(&_AstValue, (int) AST_VALUE, (int) self->dtype, n);
            break;
        };
        case FLOAT_T:{
            float f = *((float*) self->data);
            ret = new(&_AstValue, (int) AST_VALUE, (int) self->dtype, f);
            break;
        };
        case BOOL_T:{
            int n = *((int*) self->data);
            ret = new(&_AstValue, (int) AST_VALUE, (int) self->dtype, n);
            break;
        };
    }
    return ret;
}

void AstValue_innerCtor(AstValue *self, va_list *app) {
    self->dtype = (dataType) va_arg(*app, int);
    switch (self->dtype) {
        case STRING_T: {
            char *strDataTmp = va_arg(*app, char*);
            self->data = malloc(sizeof(char) * (strlen(strDataTmp) + 1));
            strcpy((char *) self->data, strDataTmp);
            break;
        }
        case INT32_T: {
            int intDataTmp = va_arg(*app, int);
            self->data = malloc(sizeof(int));
            *(int *) self->data = intDataTmp;
            break;
        }
        case FLOAT_T: {
            float floatDataTmp = (float) va_arg(*app, double);
            self->data = malloc(sizeof(float));
            *(float *) self->data = floatDataTmp;
            break;
        }
        case BOOL_T: {
            int b = va_arg(*app, int);
            self->data = malloc(sizeof(int));
            if (b) {
                *(int *) self->data = 1;
            } else {
                *(int *) self->data = 0;
            }
            break;
        }
    }
    self->toString = AstValue_toString;
};

void AstValue_ctor(void *_self, va_list *app) {
    AstValue *self = _AstNode.ctor(_self, app);
    AstValue_innerCtor(self, app);
}

void AstValue_dtor(void *_self) {
    AstValue *self = _self;
    free(self->data);
    _AstNode.dtor(_self);
}

/* -----------------------------------------------------------------
 * AST statementImm
 */
char *AstStatementImm_toString(AstStatementImm *self) {
    char start[] = "statementType: ";
    char *ret = malloc(sizeof(char) * strlen(start) + 1);
    strcpy(ret, start);
    char *tmp = getStatementTypeStr(self->base.type);
    ret = concat(ret, tmp);
    free(tmp);
    ret = concat(ret, ", value: {");
    tmp = self->value->toString(self->value);
    ret = concat(ret, tmp);
    ret = concat(ret, "}");
    free(tmp);
    return ret;
}
void* AstStatementImm_clone(void *_self) {
    AstStatementImm* self = (AstStatementImm*) _self;
    AstStatementImm* ret = new(&_AstStatementImm, (int) AST_STATEMENT, (int) IMMVAL, self->value);
    return ret;
}

void AstStatementImm_objCtor(AstStatementImm *self, va_list *app) {
    AstValue* val = (AstValue*) va_arg(*app, void*);
    self->value = _AstValue.clone(val);
    self->toString = AstStatementImm_toString;
};

void AstStatementImm_ctor(void *_self, va_list *app) {
    AstStatementImm *self = _AstStatement.ctor(_self, app);
    AstStatementImm_objCtor(self, app);
}

void AstStatementImm_dtor(void *_self) {
    AstStatementImm *self = _self;
    _AstValue.dtor(self->value);
    free(self->value);
    _AstNode.dtor(_self);
}

/* -----------------------------------------------------------------
 * AST statement column
 */
char *AstStatementColumn_toString(AstStatementColumn *self) {
    char start[] = "statementType: ";
    char *ret = malloc(sizeof(char) * strlen(start) + 1);
    strcpy(ret, start);
    char *tmp = getStatementTypeStr(self->base.type);
    ret = concat(ret, tmp);
    free(tmp);
    tmp = self->column->toString(self->column);
    ret = concat(ret, ", column: {");
    ret = concat(ret, tmp);
    free(tmp);
    ret = concat(ret, "}");
    return ret;
}

void* AstStatementColumn_clone(void *_self) {
    AstStatementColumn* self = (AstStatementColumn*) _self;

    AstStatementColumn* ret;
    if(self->column->isTableNamed){
        ret = new(&_AstStatementColumn, (int) AST_STATEMENT, (int) COLUMN,
                                      AST_COLUMN, self->column->columnName, 1, self->column->tableName);
    } else{
        ret = new(&_AstStatementColumn, (int) AST_STATEMENT, (int) COLUMN,
                                      AST_COLUMN, self->column->columnName, 0);
    }
    return ret;
}

void AstStatementColumn_innerCtor(AstStatementColumn *self, va_list *app) {
    AstColumn *newColumn = calloc(1, _AstColumn.size);
    *(const struct Class **) newColumn = &_AstColumn;
    _AstColumn.ctor(newColumn, app);

    self->column = newColumn;
    self->toString = AstStatementColumn_toString;
}

void AstStatementColumn_ctor(void *_self, va_list *app) {
    AstStatementColumn *self = _AstStatement.ctor(_self, app);
    AstStatementColumn_innerCtor(self, app);
}

void AstStatementColumn_dtor(void *_self) {
    AstStatementColumn *self = _self;
    _AstColumn.dtor(self->column);
    free(self->column);
    _AstNode.dtor(_self);
}

void AstStatementUnary_innerCtor(AstStatementUnary *self, va_list *app);
void* AstStatementBinary_clone(void *_self);
char *AstStatementBinary_toString(AstStatementBinary *self);
void AstStatementBinary_innerCtor(AstStatementBinary *self, va_list *app);
/* -----------------------------------------------------------------
 * AST statement unary
 */
char *AstStatementUnary_toString(AstStatementUnary *self) {
    char start[] = "statementType: ";
    char *ret = malloc(sizeof(char) * strlen(start) + 1);
    strcpy(ret, start);
    char *tmp = getStatementTypeStr(self->base.type);
    ret = concat(ret, tmp);
    free(tmp);
    ret = concat(ret, ", operation: ");
    tmp = getOpTypeStr(self->operation);
    ret = concat(ret, tmp);
    free(tmp);
    ret = concat(ret, ", operand: {");

    switch (self->operand->type) {
        case IMMVAL:
            tmp = AstStatementImm_toString((AstStatementImm *) self->operand);
            break;
        case COLUMN:
            tmp = AstStatementColumn_toString((AstStatementColumn *) self->operand);
            break;
        case UNARY:
            tmp = AstStatementUnary_toString((AstStatementUnary *) self->operand);
            break;
        case BINARY:
            tmp = AstStatementBinary_toString((AstStatementBinary *) self->operand);
            break;
    }
    ret = concat(ret, tmp);
    ret = concat(ret, "}");
    free(tmp);

    return ret;
}

void* AstStatementUnary_clone(void *_self) {
    AstStatementUnary* self = (AstStatementUnary*) _self;

    AstStatementUnary* ret;
    ret = new(&_AstStatementUnary, AST_STATEMENT, UNARY, 1, self->operation, self->operand);
    return ret;
}

void AstStatementUnary_immCtor(AstStatementUnary *self, va_list *app) {
    self->operation = (opType) va_arg(*app, int);
    AstStatement *statement = calloc(1, _AstStatement.size);
    *(const struct Class **) statement = &_AstStatement;
    statement = _AstStatement.ctor(statement, app);
    switch (statement->type) {
        case IMMVAL: {
            statement = realloc(statement, _AstStatementImm.size);
            AstStatementImm *imm = (AstStatementImm *) statement;
            AstStatementImm_objCtor(imm, app);
            break;
        }
        case COLUMN: {
            statement = realloc(statement, _AstStatementColumn.size);
            AstStatementColumn *column = (AstStatementColumn *) statement;
            AstStatementColumn_innerCtor(column, app);
            break;
        }
        case UNARY: {
            statement = realloc(statement, _AstStatementUnary.size);
            AstStatementUnary *unary = (AstStatementUnary *) statement;
            AstStatementUnary_innerCtor(unary, app);
            break;
        }
        case BINARY: {
            statement = realloc(statement, _AstStatementBinary.size);
            AstStatementBinary *unary = (AstStatementBinary *) statement;
            AstStatementBinary_innerCtor(unary, app);
            break;
        }
    }
    self->operand = statement;
    self->toString = AstStatementUnary_toString;
};

void AstStatementUnary_objCtor(AstStatementUnary *self, va_list *app) {
    self->operation = (opType) va_arg(*app, int);

    AstStatement *statement = (AstStatement*) va_arg(*app, void *);
    switch (statement->type) {
        case IMMVAL: {
            self->operand = _AstStatementImm.clone(statement);
            break;
        }
        case COLUMN: {
            self->operand = _AstStatementColumn.clone(statement);
            break;
        }
        case UNARY: {
            self->operand = _AstStatementUnary.clone(statement);
            break;
        }
        case BINARY: {
            self->operand = _AstStatementBinary.clone(statement);
            break;
        }
    }
    self->toString = AstStatementUnary_toString;
};

void AstStatementUnary_innerCtor(AstStatementUnary *self, va_list *app) {
    int ctorVariant = va_arg(*app, int);
    if(ctorVariant == 1){
        AstStatementUnary_objCtor(self, app);
    } else AstStatementUnary_immCtor(self, app);
};

void AstStatementUnary_ctor(void *_self, va_list *app) {
    AstStatementUnary *self = _AstStatement.ctor(_self, app);
    AstStatementUnary_innerCtor(self, app);
}

void AstStatementUnary_dtor(void *_self) {
    AstStatementUnary *self = _self;
    switch (self->operand->type) {
        case IMMVAL:
            _AstStatementImm.dtor((AstStatementImm *) self->operand);
            free(self->operand);
            break;
        case COLUMN:
            _AstStatementColumn.dtor((AstStatementColumn *) self->operand);
            free(self->operand);
            break;
        case UNARY:
            _AstStatementUnary.dtor((AstStatementUnary *) self->operand);
            free(self->operand);
            break;
        case BINARY:
            _AstStatementBinary.dtor((AstStatementBinary *) self->operand);
            free(self->operand);
            break;
    }
    _AstStatement.dtor(_self);
}

/* -----------------------------------------------------------------
 * AST statement binary
 */
char *AstStatementBinary_toString(AstStatementBinary *self) {
    char start[] = "statementType: ";
    char *ret = malloc(sizeof(char) * strlen(start) + 1);
    strcpy(ret, start);
    char *tmp = getStatementTypeStr(self->base.type);
    ret = concat(ret, tmp);
    free(tmp);
    ret = concat(ret, ", operation: ");
    tmp = getOpTypeStr(self->operation);
    ret = concat(ret, tmp);
    free(tmp);

    ret = concat(ret, ", leftOperand: {");
    switch (self->leftOperand->type) {
        case IMMVAL:
            tmp = AstStatementImm_toString((AstStatementImm *) self->leftOperand);
            break;
        case COLUMN:
            tmp = AstStatementColumn_toString((AstStatementColumn *) self->leftOperand);
            break;
        case UNARY:
            tmp = AstStatementUnary_toString((AstStatementUnary *) self->leftOperand);
            break;
        case BINARY:
            tmp = AstStatementBinary_toString((AstStatementBinary *) self->leftOperand);
            break;
    }
    ret = concat(ret, tmp);
    free(tmp);

    ret = concat(ret, "}, rightOperand: {");
    switch (self->rightOperand->type) {
        case IMMVAL:
            tmp = AstStatementImm_toString((AstStatementImm *) self->rightOperand);
            break;
        case COLUMN:
            tmp = AstStatementColumn_toString((AstStatementColumn *) self->rightOperand);
            break;
        case UNARY:
            tmp = AstStatementUnary_toString((AstStatementUnary *) self->rightOperand);
            break;
        case BINARY:
            tmp = AstStatementBinary_toString((AstStatementBinary *) self->rightOperand);
            break;
    }
    ret = concat(ret, tmp);
    ret = concat(ret, "}");
    free(tmp);

    return ret;
}

void* AstStatementBinary_clone(void *_self) {
    AstStatementBinary* self = (AstStatementBinary*) _self;

    AstStatementBinary* ret;
    ret = new(&_AstStatementBinary, AST_STATEMENT, BINARY, 1, self->operation,
              self->leftOperand, self->rightOperand);
    return ret;
}

void AstStatementBinary_immCtor(AstStatementBinary *self, va_list *app) {
    self->operation = (opType) va_arg(*app, int);

    AstStatement *lStatement = calloc(1, _AstStatement.size);
    *(const struct Class **) lStatement = &_AstStatement;
    lStatement = _AstStatement.ctor(lStatement, app);
    switch (lStatement->type) {
        case IMMVAL: {
            lStatement = realloc(lStatement, _AstStatementImm.size);
            AstStatementImm *imm = (AstStatementImm *) lStatement;
            AstStatementImm_objCtor(imm, app);
            break;
        }
        case COLUMN: {
            lStatement = realloc(lStatement, _AstStatementColumn.size);
            AstStatementColumn *column = (AstStatementColumn *) lStatement;
            AstStatementColumn_innerCtor(column, app);
            break;
        }
        case UNARY: {
            lStatement = realloc(lStatement, _AstStatementUnary.size);
            AstStatementUnary *unary = (AstStatementUnary *) lStatement;
            AstStatementUnary_innerCtor(unary, app);
            break;
        }
        case BINARY: {
            lStatement = realloc(lStatement, _AstStatementBinary.size);
            AstStatementBinary *binary = (AstStatementBinary *) lStatement;
            AstStatementBinary_innerCtor(binary, app);
            break;
        }
    }
    self->leftOperand = lStatement;

    AstStatement *rStatement = calloc(1, _AstStatement.size);
    *(const struct Class **) rStatement = &_AstStatement;
    rStatement = _AstStatement.ctor(rStatement, app);
    switch (rStatement->type) {
        case IMMVAL: {
            rStatement = realloc(rStatement, _AstStatementImm.size);
            AstStatementImm *imm = (AstStatementImm *) rStatement;
            AstStatementImm_objCtor(imm, app);
            break;
        }
        case COLUMN: {
            rStatement = realloc(rStatement, _AstStatementColumn.size);
            AstStatementColumn *column = (AstStatementColumn *) rStatement;
            AstStatementColumn_innerCtor(column, app);
            break;
        }
        case UNARY: {
            rStatement = realloc(rStatement, _AstStatementUnary.size);
            AstStatementUnary *unary = (AstStatementUnary *) rStatement;
            AstStatementUnary_innerCtor(unary, app);
            break;
        }
        case BINARY: {
            rStatement = realloc(rStatement, _AstStatementBinary.size);
            AstStatementBinary *binary = (AstStatementBinary *) rStatement;
            AstStatementBinary_innerCtor(binary, app);
            break;
        }
    }
    self->rightOperand = rStatement;

    self->toString = AstStatementBinary_toString;
}

void AstStatementBinary_objectsCtor(AstStatementBinary *self, va_list *app) {
    self->operation = (opType) va_arg(*app, int);

    AstStatement *lStatement = va_arg(*app, void*);
    switch (lStatement->type) {
        case IMMVAL:
            self->leftOperand = _AstStatementImm.clone(lStatement);
            break;
        case COLUMN:
            self->leftOperand = _AstStatementColumn.clone(lStatement);
            break;
        case UNARY:
            self->leftOperand = _AstStatementUnary.clone(lStatement);
            break;
        case BINARY:
            self->leftOperand = _AstStatementBinary.clone(lStatement);
            break;
    }
    AstStatement *rStatement = va_arg(*app, void*);
    switch (rStatement->type) {
        case IMMVAL:
            self->rightOperand = _AstStatementImm.clone(rStatement);
            break;
        case COLUMN:
            self->rightOperand = _AstStatementColumn.clone(rStatement);
            break;
        case UNARY:
            self->rightOperand = _AstStatementUnary.clone(rStatement);
            break;
        case BINARY:
            self->rightOperand = _AstStatementBinary.clone(rStatement);
            break;
    }

    self->toString = AstStatementBinary_toString;
}

void AstStatementBinary_innerCtor(AstStatementBinary *self, va_list *app) {
    int constructorVariant = va_arg(*app, int);
    if (constructorVariant == 0) {
        AstStatementBinary_immCtor(self, app);
    } else {
        AstStatementBinary_objectsCtor(self, app);
    }
};

void AstStatementBinary_ctor(void *_self, va_list *app) {
    AstStatementBinary *self = _AstStatement.ctor(_self, app);
    AstStatementBinary_innerCtor(self, app);
}

void AstStatementBinary_dtor(void *_self) {
    AstStatementBinary *self = _self;
    switch (self->leftOperand->type) {
        case IMMVAL:
            _AstStatementImm.dtor((AstStatementImm *) self->leftOperand);
            free(self->leftOperand);
            break;
        case COLUMN:
            _AstStatementColumn.dtor((AstStatementColumn *) self->leftOperand);
            free(self->leftOperand);
            break;
        case UNARY:
            _AstStatementUnary.dtor((AstStatementUnary *) self->leftOperand);
            free(self->leftOperand);
            break;
        case BINARY:
            _AstStatementBinary.dtor((AstStatementBinary *) self->leftOperand);
            free(self->leftOperand);
            break;
    }
    switch (self->rightOperand->type) {
        case IMMVAL:
            _AstStatementImm.dtor((AstStatementImm *) self->rightOperand);
            free(self->rightOperand);
            break;
        case COLUMN:
            _AstStatementColumn.dtor((AstStatementColumn *) self->rightOperand);
            free(self->rightOperand);
            break;
        case UNARY:
            _AstStatementUnary.dtor((AstStatementUnary *) self->rightOperand);
            free(self->rightOperand);
            break;
        case BINARY:
            _AstStatementBinary.dtor((AstStatementBinary *) self->rightOperand);
            free(self->rightOperand);
            break;
    }
    _AstStatement.dtor(_self);
}

/* -----------------------------------------------------------------
 * AST select
 */
char *AstSelect_toString(AstSelect *self) {
    char start[] = "columns: {";
    char *ret = malloc(sizeof(char) * strlen(start) + 1);
    strcpy(ret, start);
    char *tmp = self->columns->toString(self->columns);
    ret = concat(ret, tmp);
    free(tmp);

    ret = concat(ret, ", src: {");
    tmp = self->srcTable->toString(self->srcTable);
    ret = concat(ret, tmp);
    free(tmp);
    ret = concat(ret, "}");

    if (self->hasFilter) {
        ret = concat(ret, ", filter: {");
        switch (self->filter->type) {
            case BINARY:
                tmp = AstStatementBinary_toString(self->filter);
                break;
            case UNARY:
                tmp = AstStatementUnary_toString(self->filter);
                break;
            case IMMVAL:
                tmp = AstStatementImm_toString(self->filter);
                break;
            case COLUMN:
                tmp = AstStatementColumn_toString(self->filter);
                break;
        }
        ret = concat(ret, tmp);
        free(tmp);
        ret = concat(ret, "}");
    }
    return ret;
}

void AstSelect_immCtor(AstSelect *self, va_list *app) {
    AstSrcTable *newSrcTable = calloc(1, _AstSrcTable.size);
    *(const struct Class **) newSrcTable = &_AstSrcTable;
    _AstSrcTable.ctor(newSrcTable, app);
    self->srcTable = newSrcTable;

    AstColumns *newColumns = calloc(1, _AstColumns.size);
    *(const struct Class **) newColumns = &_AstColumns;
    _AstColumns.ctor(newColumns, app);
    self->columns = newColumns;

    int hasFilter = va_arg(*app, int);

    if (hasFilter) {
        self->hasFilter = true;
        AstStatement *statement = calloc(1, _AstStatement.size);
        *(const struct Class **) statement = &_AstStatement;
        statement = _AstStatement.ctor(statement, app);
        switch (statement->type) {
            case IMMVAL: {
                statement = realloc(statement, _AstStatementImm.size);
                AstStatementImm *imm = (AstStatementImm *) statement;
                AstStatementImm_objCtor(imm, app);
                break;
            }
            case COLUMN: {
                statement = realloc(statement, _AstStatementColumn.size);
                AstStatementColumn *column = (AstStatementColumn *) statement;
                AstStatementColumn_innerCtor(column, app);
                break;
            }
            case UNARY: {
                statement = realloc(statement, _AstStatementUnary.size);
                AstStatementUnary *unary = (AstStatementUnary *) statement;
                AstStatementUnary_innerCtor(unary, app);
                break;
            }
            case BINARY: {
                statement = realloc(statement, _AstStatementBinary.size);
                AstStatementBinary *binary = (AstStatementBinary *) statement;
                AstStatementBinary_innerCtor(binary, app);
                break;
            }
        }
        self->filter = statement;
    }

    self->toString = AstSelect_toString;
}

void AstSelect_objCtor(AstSelect *self, va_list *app) {
    AstColumns *cols = (AstColumns *) va_arg(*app, void *);
    self->columns = _AstColumns.clone(cols);

    AstSrcTable *src = (AstSrcTable *) va_arg(*app, void *);
    self->srcTable = _AstSrcTable.clone(src);
    int hasFilter = va_arg(*app, int);
    if (hasFilter) {
        self->hasFilter = true;
        AstStatement* st = (AstStatement *) va_arg(*app, void *);
        switch (st->type){
            case IMMVAL:
                self->filter = _AstStatementImm.clone(st);
                break;
            case COLUMN:
                self->filter = _AstStatementColumn.clone(st);
                break;
            case UNARY:
                self->filter = _AstStatementUnary.clone(st);
                break;
            case BINARY:
                self->filter = _AstStatementBinary.clone(st);
                break;
        }
    } else self->hasFilter = false;
    self->toString = AstSelect_toString;
}

void AstSelect_ctor(void *_self, va_list *app) {
    AstSelect *self = _AstNode.ctor(_self, app);
    int constructorVariant = va_arg(*app, int);
    if (constructorVariant == 0) {
        AstSelect_immCtor(self, app);
    } else {
        AstSelect_objCtor(self, app);
    }
}

void AstSelect_dtor(void *_self) {
    AstSelect *self = _self;

    _AstSrcTable.dtor(self->srcTable);
    free(self->srcTable);

    _AstColumns.dtor(self->columns);
    free(self->columns);

    if (self->hasFilter) {
        switch (self->filter->type) {
            case IMMVAL:
                _AstStatementImm.dtor((AstStatementImm *) self->filter);
                free(self->filter);
                break;
            case COLUMN:
                _AstStatementColumn.dtor((AstStatementColumn *) self->filter);
                free(self->filter);
                break;
            case UNARY:
                _AstStatementUnary.dtor((AstStatementUnary *) self->filter);
                free(self->filter);
                break;
            case BINARY:
                _AstStatementBinary.dtor((AstStatementBinary *) self->filter);
                free(self->filter);
                break;
        }
    }
    _AstNode.dtor(_self);
}

char *callToString(AstNode *ast) {
    switch (ast->type) {
        case AST_SELECT:
            return AstSelect_toString((AstSelect *) ast);
    }
}

char *callDelete(AstNode *ast) {
    switch (ast->type) {
        case AST_SELECT:
            _AstSelect.dtor((AstSelect *) ast);
            free(ast);
            break;
        default:
            fprintf(stderr, "wrong ast type to delete\n");
    }
}
