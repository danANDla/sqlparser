//
// Created by danandla on 3/15/23.
//

#ifndef SQLPARSER_SQLPARSER_H
#define SQLPARSER_SQLPARSER_H

#include "ast.h"

int parse_query(const char* str, AstNode** result);

#endif //SQLPARSER_SQLPARSER_H
